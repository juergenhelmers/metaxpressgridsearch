class PlatePropertiesController < ApplicationController
  # GET /plate_properties
  # GET /plate_properties.xml
  def index
    @plate_properties = PlateProperty.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @plate_properties }
    end
  end

  # GET /plate_properties/1
  # GET /plate_properties/1.xml
  def show
    @plate_property = PlateProperty.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @plate_property }
    end
  end

  # GET /plate_properties/new
  # GET /plate_properties/new.xml
  def new
    @plate_property = PlateProperty.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @plate_property }
    end
  end

  # GET /plate_properties/1/edit
  def edit
    @plate_property = PlateProperty.find(params[:id])
  end

  # POST /plate_properties
  # POST /plate_properties.xml
  def create
    @plate_property = PlateProperty.new(params[:plate_property])

    respond_to do |format|
      if @plate_property.save
        flash[:notice] = 'PlateProperty was successfully created.'
        format.html { redirect_to(@plate_property) }
        format.xml  { render :xml => @plate_property, :status => :created, :location => @plate_property }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @plate_property.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /plate_properties/1
  # PUT /plate_properties/1.xml
  def update
    @plate_property = PlateProperty.find(params[:id])

    respond_to do |format|
      if @plate_property.update_attributes(params[:plate_property])
        flash[:notice] = 'PlateProperty was successfully updated.'
        format.html { redirect_to(@plate_property) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @plate_property.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /plate_properties/1
  # DELETE /plate_properties/1.xml
  def destroy
    @plate_property = PlateProperty.find(params[:id])
    @plate_property.destroy

    respond_to do |format|
      format.html { redirect_to(plate_properties_url) }
      format.xml  { head :ok }
    end
  end
end
