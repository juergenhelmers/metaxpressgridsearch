class ImageSourcesController < ApplicationController
  # GET /image_sources
  # GET /image_sources.xml
  def index
    @image_sources = ImageSource.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @image_sources }
    end
  end

  # GET /image_sources/1
  # GET /image_sources/1.xml
  def show
    @image_source = ImageSource.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @image_source }
    end
  end

  # GET /image_sources/new
  # GET /image_sources/new.xml
  def new
    @image_source = ImageSource.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @image_source }
    end
  end

  # GET /image_sources/1/edit
  def edit
    @image_source = ImageSource.find(params[:id])
  end

  # POST /image_sources
  # POST /image_sources.xml
  def create
    @image_source = ImageSource.new(params[:image_source])

    respond_to do |format|
      if @image_source.save
        flash[:notice] = 'ImageSource was successfully created.'
        format.html { redirect_to(@image_source) }
        format.xml  { render :xml => @image_source, :status => :created, :location => @image_source }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @image_source.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /image_sources/1
  # PUT /image_sources/1.xml
  def update
    @image_source = ImageSource.find(params[:id])

    respond_to do |format|
      if @image_source.update_attributes(params[:image_source])
        flash[:notice] = 'ImageSource was successfully updated.'
        format.html { redirect_to(@image_source) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @image_source.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /image_sources/1
  # DELETE /image_sources/1.xml
  def destroy
    @image_source = ImageSource.find(params[:id])
    @image_source.destroy

    respond_to do |format|
      format.html { redirect_to(image_sources_url) }
      format.xml  { head :ok }
    end
  end
end
