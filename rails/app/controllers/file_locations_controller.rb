class FileLocationsController < ApplicationController
  # GET /file_locations
  # GET /file_locations.xml
  def index
    @file_locations = FileLocation.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @file_locations }
    end
  end

  # GET /file_locations/1
  # GET /file_locations/1.xml
  def show
    @file_location = FileLocation.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @file_location }
    end
  end

  # GET /file_locations/new
  # GET /file_locations/new.xml
  def new
    @file_location = FileLocation.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @file_location }
    end
  end

  # GET /file_locations/1/edit
  def edit
    @file_location = FileLocation.find(params[:id])
  end

  # POST /file_locations
  # POST /file_locations.xml
  def create
    @file_location = FileLocation.new(params[:file_location])

    respond_to do |format|
      if @file_location.save
        flash[:notice] = 'FileLocation was successfully created.'
        format.html { redirect_to(@file_location) }
        format.xml  { render :xml => @file_location, :status => :created, :location => @file_location }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @file_location.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /file_locations/1
  # PUT /file_locations/1.xml
  def update
    @file_location = FileLocation.find(params[:id])

    respond_to do |format|
      if @file_location.update_attributes(params[:file_location])
        flash[:notice] = 'FileLocation was successfully updated.'
        format.html { redirect_to(@file_location) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @file_location.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /file_locations/1
  # DELETE /file_locations/1.xml
  def destroy
    @file_location = FileLocation.find(params[:id])
    @file_location.destroy

    respond_to do |format|
      format.html { redirect_to(file_locations_url) }
      format.xml  { head :ok }
    end
  end
end
