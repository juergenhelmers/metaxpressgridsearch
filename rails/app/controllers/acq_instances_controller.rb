class AcqInstancesController < ApplicationController
  # GET /acq_instances
  # GET /acq_instances.xml
  def index
    @acq_instances = AcqInstance.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @acq_instances }
    end
  end

  # GET /acq_instances/1
  # GET /acq_instances/1.xml
  def show
    @acq_instance = AcqInstance.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @acq_instance }
    end
  end

  # GET /acq_instances/new
  # GET /acq_instances/new.xml
  def new
    @acq_instance = AcqInstance.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @acq_instance }
    end
  end

  # GET /acq_instances/1/edit
  def edit
    @acq_instance = AcqInstance.find(params[:id])
  end

  # POST /acq_instances
  # POST /acq_instances.xml
  def create
    @acq_instance = AcqInstance.new(params[:acq_instance])

    respond_to do |format|
      if @acq_instance.save
        flash[:notice] = 'AcqInstance was successfully created.'
        format.html { redirect_to(@acq_instance) }
        format.xml  { render :xml => @acq_instance, :status => :created, :location => @acq_instance }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @acq_instance.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /acq_instances/1
  # PUT /acq_instances/1.xml
  def update
    @acq_instance = AcqInstance.find(params[:id])

    respond_to do |format|
      if @acq_instance.update_attributes(params[:acq_instance])
        flash[:notice] = 'AcqInstance was successfully updated.'
        format.html { redirect_to(@acq_instance) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @acq_instance.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /acq_instances/1
  # DELETE /acq_instances/1.xml
  def destroy
    @acq_instance = AcqInstance.find(params[:id])
    @acq_instance.destroy

    respond_to do |format|
      format.html { redirect_to(acq_instances_url) }
      format.xml  { head :ok }
    end
  end
end
