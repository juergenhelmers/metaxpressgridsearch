class FileLocation < ActiveRecord::Base
  set_table_name "file_location"
  set_primary_key :LOCATION_ID   
  belongs_to :acq_instance
end
