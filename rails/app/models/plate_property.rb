class PlateProperty < ActiveRecord::Base
  set_table_name "plate_property"
  has_one :plate
end
