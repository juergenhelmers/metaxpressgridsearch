class Plate < ActiveRecord::Base
  
  has_one :plate_property
  
  set_primary_key :PLATE_ID

  belongs_to :user, :foreign_key => "CREATOR_ID"
end
