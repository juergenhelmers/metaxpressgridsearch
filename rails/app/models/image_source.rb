class ImageSource < ActiveRecord::Base
  set_table_name "image_source"
  set_primary_key "IMAGE_SOURCE_ID"
  belongs_to :assay, :foreign_key => "ASSAY_ID" 
end
