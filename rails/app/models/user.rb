class User < ActiveRecord::Base
  set_primary_key :USER_LOGIN_ID
  has_many :plates, :foreign_key => "PLATE_ID"
end
