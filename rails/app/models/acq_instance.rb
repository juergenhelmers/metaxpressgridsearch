class AcqInstance < ActiveRecord::Base
  set_table_name "acq_instance"
  has_one :file_location, :foreign_key => "LOCATION_ID"
  set_primary_key :ACQ_ID
end
