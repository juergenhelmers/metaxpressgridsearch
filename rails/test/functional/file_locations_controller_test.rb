require 'test_helper'

class FileLocationsControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:file_locations)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create file_location" do
    assert_difference('FileLocation.count') do
      post :create, :file_location => { }
    end

    assert_redirected_to file_location_path(assigns(:file_location))
  end

  test "should show file_location" do
    get :show, :id => file_locations(:one).to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => file_locations(:one).to_param
    assert_response :success
  end

  test "should update file_location" do
    put :update, :id => file_locations(:one).to_param, :file_location => { }
    assert_redirected_to file_location_path(assigns(:file_location))
  end

  test "should destroy file_location" do
    assert_difference('FileLocation.count', -1) do
      delete :destroy, :id => file_locations(:one).to_param
    end

    assert_redirected_to file_locations_path
  end
end
