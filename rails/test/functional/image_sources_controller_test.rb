require 'test_helper'

class ImageSourcesControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:image_sources)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create image_source" do
    assert_difference('ImageSource.count') do
      post :create, :image_source => { }
    end

    assert_redirected_to image_source_path(assigns(:image_source))
  end

  test "should show image_source" do
    get :show, :id => image_sources(:one).to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => image_sources(:one).to_param
    assert_response :success
  end

  test "should update image_source" do
    put :update, :id => image_sources(:one).to_param, :image_source => { }
    assert_redirected_to image_source_path(assigns(:image_source))
  end

  test "should destroy image_source" do
    assert_difference('ImageSource.count', -1) do
      delete :destroy, :id => image_sources(:one).to_param
    end

    assert_redirected_to image_sources_path
  end
end
