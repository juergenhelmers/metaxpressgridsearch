require 'test_helper'

class AcqInstancesControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:acq_instances)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create acq_instance" do
    assert_difference('AcqInstance.count') do
      post :create, :acq_instance => { }
    end

    assert_redirected_to acq_instance_path(assigns(:acq_instance))
  end

  test "should show acq_instance" do
    get :show, :id => acq_instances(:one).to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => acq_instances(:one).to_param
    assert_response :success
  end

  test "should update acq_instance" do
    put :update, :id => acq_instances(:one).to_param, :acq_instance => { }
    assert_redirected_to acq_instance_path(assigns(:acq_instance))
  end

  test "should destroy acq_instance" do
    assert_difference('AcqInstance.count', -1) do
      delete :destroy, :id => acq_instances(:one).to_param
    end

    assert_redirected_to acq_instances_path
  end
end
