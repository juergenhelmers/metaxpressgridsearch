require 'test_helper'

class PlatePropertiesControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:plate_properties)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create plate_property" do
    assert_difference('PlateProperty.count') do
      post :create, :plate_property => { }
    end

    assert_redirected_to plate_property_path(assigns(:plate_property))
  end

  test "should show plate_property" do
    get :show, :id => plate_properties(:one).to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => plate_properties(:one).to_param
    assert_response :success
  end

  test "should update plate_property" do
    put :update, :id => plate_properties(:one).to_param, :plate_property => { }
    assert_redirected_to plate_property_path(assigns(:plate_property))
  end

  test "should destroy plate_property" do
    assert_difference('PlateProperty.count', -1) do
      delete :destroy, :id => plate_properties(:one).to_param
    end

    assert_redirected_to plate_properties_path
  end
end
