# Be sure to restart your server when you modify this file.

# Your secret key for verifying cookie session data integrity.
# If you change this key, all old sessions will become invalid!
# Make sure the secret is at least 30 characters and all random, 
# no regular words or you'll be exposed to dictionary attacks.
ActionController::Base.session = {
  :key         => '_metaxpress_session',
  :secret      => 'c1849d0264f3d6cf7b156716591ed5c261da5f638b7b540fe87e0a88607119e448673fc91cf62eff1f60f0aa2756de01cb493c0f566b6d707b8106bf338bea4b'
}

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rake db:sessions:create")
# ActionController::Base.session_store = :active_record_store
