# This file is auto-generated from the current state of the database. Instead of editing this file, 
# please use the migrations feature of Active Record to incrementally modify your database, and
# then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your database schema. If you need
# to create the application database on another system, you should be using db:schema:load, not running
# all the migrations from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20101005090131) do

  create_table "acq_instances", :id => false, :force => true do |t|
    t.integer  "ACQ_ID"
    t.string   "ACQ_NAME"
    t.text     "ACQ_DESC"
    t.binary   "ACQ_DATA"
    t.integer  "DATA_SIZE"
    t.integer  "LOCATION_ID"
    t.datetime "START_DATE"
    t.datetime "END_DATE"
    t.integer  "USER_LOGIN_ID"
    t.text     "DESCRIPTION"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "file_locations", :id => false, :force => true do |t|
    t.integer  "LOCATION_ID"
    t.string   "SERVER_NAME"
    t.string   "SERVER_ROOT"
    t.integer  "SERVER_PORT"
    t.string   "DIRECTORY"
    t.integer  "LOCATION_TYPE"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "image_sources", :id => false, :force => true do |t|
    t.integer  "IMAGE_SOURCE_ID"
    t.integer  "ACQ_ID"
    t.integer  "ASSAY_ID"
    t.text     "SOURCE_DESCRIPTION"
    t.text     "SOURCE_ILLUMINATION"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "plates", :id => false, :force => true do |t|
    t.integer  "PLATE_ID"
    t.integer  "BATCH_ID"
    t.integer  "ACQ_ID"
    t.integer  "X_WELLS"
    t.integer  "Y_WELLS"
    t.string   "BARCODE"
    t.string   "GLOBAL_ID"
    t.string   "PLATE_NAME"
    t.text     "PLATE_DESCRIPTION"
    t.datetime "TIME_CREATED"
    t.integer  "CREATOR_ID"
    t.datetime "TIME_MODIFIED"
    t.integer  "MODIFIER_ID"
    t.integer  "TO_DELETE"
    t.integer  "ANNOTATED_BY"
    t.datetime "TIME_ANNOTATED"
    t.integer  "ARCH_GLOBALID"
    t.datetime "created_at"
    t.datetime "updated_at"
  end
  
    create_table "plate_property", :id => false, :force => true do |t|
    t.integer  "PLATE_ID"
    t.string  "MDCS5"
  end

  create_table "users", :id => false, :force => true do |t|
    t.integer  "USER_LOGIN_ID"
    t.string   "LOGIN"
    t.text     "DESCR"
    t.integer  "USER_PERMISSIONS"
    t.integer  "TO_DELETE"
    t.integer  "PRIMARY_GROUP_ID"
    t.integer  "CREATOR_ID"
    t.datetime "TIME_CREATED"
    t.string   "NAME"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
