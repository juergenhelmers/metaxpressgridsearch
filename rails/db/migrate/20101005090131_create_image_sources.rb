class CreateImageSources < ActiveRecord::Migration
  def self.up
    create_table :image_sources, :id => false do |t|
      t.integer :IMAGE_SOURCE_ID
      t.integer :ACQ_ID
      t.integer :ASSAY_ID
      t.text :SOURCE_DESCRIPTION
      t.text :SOURCE_ILLUMINATION

      t.timestamps
    end
  end

  def self.down
    drop_table :image_sources
  end
end
