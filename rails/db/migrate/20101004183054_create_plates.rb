class CreatePlates < ActiveRecord::Migration
  def self.up
    create_table :plates, :id => false do |t|
      t.integer :PLATE_ID
      t.integer :BATCH_ID
      t.integer :ACQ_ID
      t.integer :X_WELLS
      t.integer :Y_WELLS
      t.string :BARCODE
      t.string :GLOBAL_ID
      t.string :PLATE_NAME
      t.text :PLATE_DESCRIPTION
      t.datetime :TIME_CREATED
      t.integer :CREATOR_ID
      t.datetime :TIME_MODIFIED
      t.integer :MODIFIER_ID
      t.integer :TO_DELETE
      t.integer :ANNOTATED_BY
      t.datetime :TIME_ANNOTATED
      t.integer :ARCH_GLOBALID

      t.timestamps
    end
  end

  def self.down
    drop_table :plates
  end
end

