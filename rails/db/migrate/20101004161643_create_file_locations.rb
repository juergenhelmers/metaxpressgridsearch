class CreateFileLocations < ActiveRecord::Migration
  def self.up
    create_table :file_locations, :id => false do |t|
      t.integer :LOCATION_ID
      t.string :SERVER_NAME
      t.string :SERVER_ROOT
      t.integer :SERVER_PORT
      t.string :DIRECTORY
      t.integer :LOCATION_TYPE

      t.timestamps
    end
  end

  def self.down
    drop_table :file_locations
  end
end

