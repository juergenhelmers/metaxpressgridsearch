class CreateAcqInstances < ActiveRecord::Migration
  def self.up
    create_table :acq_instances, :id => false do |t|
      t.integer :ACQ_ID
      t.string :ACQ_NAME
      t.text :ACQ_DESC
      t.binary :ACQ_DATA
      t.integer :DATA_SIZE
      t.integer :LOCATION_ID
      t.datetime :START_DATE
      t.datetime :END_DATE
      t.integer :USER_LOGIN_ID
      t.text :DESCRIPTION
      t.int :TO_DELETE

      t.timestamps
    end
  end

  def self.down
    drop_table :acq_instances
  end
end

