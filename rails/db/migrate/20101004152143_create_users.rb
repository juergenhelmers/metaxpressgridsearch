class CreateUsers < ActiveRecord::Migration
  def self.up
    create_table :users, :id => false do |t|
      t.integer :USER_LOGIN_ID
      t.string :LOGIN
      t.text :DESCR
      t.integer :USER_PERMISSIONS
      t.integer :TO_DELETE
      t.integer :PRIMARY_GROUP_ID
      t.integer :CREATOR_ID
      t.datetime :TIME_CREATED
      t.string :NAME

      t.timestamps
    end
  end

  def self.down
    drop_table :users
  end
end

