#!/usr/bin/env ruby

require 'rubygems' # comment this line out with ruby 1.9
require 'escape'
require 'rexml/document'
require 'uuidtools'
require 'pp'
require 'pg'
require 'date'
require 'odbc'
require 'dbi'
require 'optparse'
require 'pp'
require 'net/http'
require 'json'

require File.pand_path(File.dirname(__FILE__) + '/modules/parse.rb')
require File.pand_path(File.dirname(__FILE__) + '/modules/databases.rb')
require File.pand_path(File.dirname(__FILE__) + '/modules/generate.rb')

# this script transforms .tif images and some metadata into ome-tif tifs valid for omero
# - read all tifs in ./input
# - enrich metadata from input file if data
# - enrich metadata from ms sql connection to MetaExpress database
# - write ome-tifs in output

class Transfer

  def initialize(opts = {})

    @options = opts
    @metaexpress_metadata = {}
    @metadata = {}
    @all_errors = {} # collect errors for failure mail text

    @use_database = opts[:use_dbs] if !opts[:use_dbs].nil?

    @omero_db_connection = {:host=>opts[:omero_host],
                             :user=>opts[:omero_database_username],
                             :password=>opts[:omero_database_password],
                             :dbname=>opts[:omero_database_name]
    }
    @metaexpress_db_connection = {:connection => opts[:metaxpress_connection],
                                   :user => opts[:metaexpress_database_username],
                                   :password =>opts[:metaexpress_database_password]
    }

    if @use_database
      @db = Databases.new(@omero_db_connection,@metaexpress_db_connection)
    end
    
  end

  def uuid_from_filename(basename)
    #20100825 rbilly_A01_w1E3AF3878-C79B-4C1C-B5C7-E98DA18167AF.tif
    date, rest = basename.split(" ")
    name, pic_id, uuid = rest.split("_")
    uuid = uuid[2..uuid.length] # strip wX from uuid start
  end

  def generate_uuid
    return UUIDTools::UUID.timestamp_create().to_s
  end

  def enqueue_default(files, docs, dataset_id, omero_host)

    begin
      @host = 'localhost'
      @port = '9292'
      @post_ws = "/"
      @payload ={
          "command" => "import",
          #"files" => ["f 1 .tif", "f 2", "f  3"],
          "auth_key" => "gghhddeeffss7657654gff54444"
      }
      @payload["files"] = files
      @payload["docs"] = docs
      @payload["dataset_id"] = dataset_id
      @payload["omero_host"] = omero_host
      @payload["input_directory"] = @options[:input_directory]
      @payload["output_directory"] = @options[:output_directory]


      req = Net::HTTP::Post.new(@post_ws, {'Content-Type' =>'application/json'})
      req.body = @payload.to_json
      response = Net::HTTP.new(@host, @port).start { |http| http.request(req) }
      case response
        when Net::HTTPSuccess # , Net::HTTPRedirection
          # OK
        else
          p "Omero Import Manager unavailable! Error:" + response.error
      end
    rescue
      p "Omero Import Manager unavailable at http://#{@host}:#{@port}/ Error: " + "#{$!}"
      exit
    end

  end

  def enqueue_reassignment(experimenter, dataset_id, final_project_id)

    begin
      @host = 'localhost'
      @port = '9292'
      @post_ws = "/reassign"
      @payload ={
          "command" => "reassignment",
          "auth_key" => "gghhddeeffss7657654gff54444"
      }
      @payload["experimenter"] = experimenter
      @payload["dataset_id"] = dataset_id
      @payload["final_project_id"] = final_project_id

      req = Net::HTTP::Post.new(@post_ws, {'Content-Type' =>'application/json'})
      req.body = @payload.to_json
      response = Net::HTTP.new(@host, @port).start { |http| http.request(req) }
      case response
        when Net::HTTPSuccess # , Net::HTTPRedirection
          # OK
        else
          p "Omero Reassignment unavailable! Error:" + response.error
      end
    rescue
      p "Omero Reassignment unavailable at http://#{@host}:#{@port}/ Error: " + "#{$!}"
      exit
    end

  end

  def expand_dir(dir)
    if !dir.start_with?("/")
      if dir.start_with?("./")
        dir = Dir.pwd + dir[1..dir.length]
      else
        dir = Dir.pwd + '/' + dir
      end
    end
    dir
  end

  def expand_and_clean_pathnames
    # make sure the path ends with /
    @options[:output_directory] = @options[:output_directory]+'/' unless @options[:output_directory].end_with?("/")
    @options[:input_directory] = @options[:input_directory]+'/' unless @options[:input_directory].end_with?("/")

    # expand relative output path - import workers need full path
    @options[:output_directory] = expand_dir(@options[:output_directory])
    @options[:input_directory] = expand_dir(@options[:input_directory])
  end

  def process

    expand_and_clean_pathnames

    # iterates over all files in input dir and processes them
    filetypes = File.join(@options[:input_directory], "**", "*.tif")
    files = Dir[filetypes].sort
    plate_id = 0
    @dataset_id = 1
    @file_batch = []
    @doc_batch = []

    files.each_with_index { |file, index| #try/catch block around every file processing
      begin

        @options[:basename] = File.basename(file, ".tif")
        @options[:relative_filename_input] = file.gsub(@options[:input_directory],"")
        @options[:relative_filename_output] = @options[:relative_filename_input].gsub(".tif",".ome.tif")
        @options[:input_filename] = @options[:input_directory]+@options[:relative_filename_input]
        @options[:output_filename] = @options[:output_directory]+@options[:relative_filename_output]

        #extract exif metadata into result hash
        command = Escape.shell_command(["exiv2", '-pa', @options[:input_filename]])
        result = %x[#{command}]
        # check for errors of exiv2 command
        # - exiv2 has an strange result code on exit (64768), not null. maybe a bug?
        # - but conversion is ok. therefor we check for this exit code also
        throw "exiv2 failed for image "+@options[:input_filename]+" Error: " + $?.to_s + " Command: " + command unless $?.success? or $?.to_i == 64768
        @metadata = Parse.exif_tags(result)

        # analyze metaexpress flat file data and generate nice hash value => key pairs
        @metaexpress_metadata = Parse.unformatted_metaexpress_metadata(@metadata["ImageDescription"])

        # merge metadata from exif and metaexpress ImageDescription
        @metadata.merge!(@metaexpress_metadata)
        @metadata.delete(nil) #cleanup nil error

        if @options[:input_filename].include?("_A01_") # we have the first picture of a dataset
          #put current plate id into result hash of files that are not the first image (eg. *A01*)
          plate_id = @metadata["Plate ID"]
          # get metaXpress user name from plateID
          #metaXpress_login = get_user_id(metadata["Plate ID"])
          if @use_database
            @metaxpress_login = @db.get_metaexpress_user_id(@metadata["Plate ID"])
            @experimenter = @db.get_omero_experimenter(@metaxpress_login)
            # check if we have already assigned and created a dataset
            if @dataset_id.nil?

              #define the importing Omero account. TODO: Move that into a script parameter?
              @importer = @db.get_importer(@experimenter)
              @final_project_id = @db.check_for_project(@metadata, @experimenter)
              #create a new dataset for importer
              @dataset_id = @db.create_dataset(@metadata, @importer)
            end
          else
            @metaxpress_login = 3
            @final_project_id = 1
            @experimenter = {"id"=>"3", "firstname"=>"Juergen", "lastname"=>"Helmers", "email"=>"juergen.helmers@gmail.com", "institution"=>"CSIR"}
          end
        else #plate id has already been extracted from first image of dataset
          @metadata["Plate ID"] = plate_id
        end

        # correct date formatting: 2010:08:25 16:42:59 => 2010-08-25T16:42:59
        datetime = DateTime.strptime(@metadata["DateTime"], '%Y:%m:%d %H:%M:%S')
        @metadata["DateTime"] = datetime.strftime("%Y-%m-%dT%H:%M:%S")

        # generate a new OME-TIF xml from available metadata
        image_number = 1
        doc = Generate.generate_ome_xml(@metadata, @metaxpress_login, @experimenter, image_number, generate_uuid, @options[:basename]+".tif")

        @file_batch << @options[:relative_filename_input]
        @doc_batch << doc
        if index > 0 && index%250 == 0
          p "enqueing file batch,  #{index}"
          enqueue_default(@file_batch, @doc_batch, @dataset_id, @options[:omero_host])
          @file_batch = []
          @doc_batch = []
        end
        # debugging
        #  show metadata hash
        #PP::pp(@metadata, $stderr, 50)
        #  show resulting OME XML document
        #doc.write
      rescue
        # handle all errors that crop up in image processing
        #  TODO maybe handle errors/exceptions finer
        error_message = "#{$!}"
        @all_errors[file] = error_message
      end }
    enqueue_default(@file_batch, @doc_batch, @dataset_id, @options[:omero_host])
    @file_batch = []
    @doc_batch = []
    if @all_errors.length > 0
      #TODO send error hash to admin via shell call to mail program
      PP::pp("Error Count: "+@all_errors.length.to_s, $stderr, 50)
      PP::pp(@all_errors, $stderr, 50)
    end

    # enqueue the final job to different job queue with lower priority to be run last
    enqueue_reassignment(@experimenter, @dataset_id, @final_project_id)
    if @all_errors.length > 0
      #TODO send error hash to admin via shell call to mail program
      PP::pp("Error Count: "+@all_errors.length.to_s, $stderr, 50)
      PP::pp(@all_errors, $stderr, 50)
    end
  end

end


class String
  def remove_first_line!
    first_newline = (index("\n") || size - 1) + 1
    slice!(0, first_newline).sub("\n", '')
  end

  def to_b
    return [true, "true", 1, "1", "T", "t"].include?(self.class == String ? self.downcase : self)
  end

end

REXML::Attribute.class_eval(%q^
  def to_string
    %Q[#@expanded_name="#{to_s().gsub(/"/, '&quot;')}"]
  end
^)

options = {}

optparse = OptionParser.new do |opts|
  # Set a banner, displayed at the top of the help screen.
  opts.banner = "Usage: transfer.rb [options]"

  # Define the options, and what they do
  options[:verbose] = false
  opts.on('-v', '--verbose', 'Output more information') do
    options[:verbose] = true
  end

  options[:input_directory] = './input/'
  opts.on('-i', '--input_directory DIR', 'Set input directory with metaxpress images') do |dir|
    options[:input_directory] = dir
  end

  options[:output_directory] = './output/'
  opts.on('-o', '--output_directory DIR', 'Set output directory for ome-xml images. Directoy must exist!!!') do |dir|
    options[:output_directory] = dir
  end

  options[:omero_host] = 'localhost'
  opts.on('-s', '--omero_host HOST', 'Set omero database host') do |host|
    options[:omero_host] = host
  end

  options[:omero_database_name] = 'omero'
  opts.on('-n', '--omero_database_name NAME', 'Set omero database name') do |name|
    options[:omero_database_name] = name
  end

  options[:omero_database_username] = 'omero'
  opts.on('-u', '--omero_database_username USERNAME', 'Set omero database username') do |username|
    options[:omero_database_username] = username
  end

  options[:omero_database_password] = 'IOP999jkl'
  opts.on('-p', '--omero_database_password PASSWORD', 'Set omero database password') do |password|
    options[:omero_database_password] = password
  end

  options[:metaxpress_connection] = 'dbi:ODBC:metaxpress'
  opts.on('-C', '--metaxpress_connection CONNECTION_STRING', 'Set metaxpress connection string (example: dbi:ODBC:metaxpress)') do |connection_string|
    options[:metaxpress_connection] = connection_string
  end

  options[:metaexpress_database_username] = 'sa'
  opts.on('-U', '--metaexpress_database_username USERNAME', 'Set metaxpress database username') do |username|
    options[:metaexpress_database_username] = username
  end

  options[:metaexpress_database_password] = 'MDC'
  opts.on('-P', '--metaexpress_database_password PASSWORD', 'Set metaxpress database password') do |password|
    options[:metaexpress_database_password] = password
  end

  options[:use_dbs] = true
  opts.on('-d', '--use_databases VAL', 'Use databases for omero and metaxpress [true|false]') do |val|
    options[:use_dbs] = val.to_b
  end

  options[:logfile] = nil
  opts.on('-l', '--logfile FILE', 'Write log to FILE') do |file|
    options[:logfile] = file
  end

  # This displays the help screen, all programs are
  # assumed to have this option.
  opts.on('-h', '--help', 'Display this screen') do
    puts opts
    exit
  end
end

# 'parse' method simply parses ARGV
# 'parse!' method parses ARGV and removes any options found there, as well as any parameters for
# the options.
optparse.parse!

puts "Being verbose" if options[:verbose]
puts "Logging to file #{options[:logfile]}" if options[:logfile]

# run the transfer (without optional args)
transferables = Transfer.new(options)
transferables.process

# use loop below if unused opts (fe. filenames) should be used/looped
#ARGV.each do|f|
#end
