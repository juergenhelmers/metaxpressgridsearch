require 'pp'

@@example_text = %{Experiment base name:20101124  big head LamA 2 bin2x2
Experiment set:Optimization of encapsulation mix
XPO1


overlaid with ATCC Hela
for 48 hours

1 x confluent T25 around three million cells per array

standard fix and label methods

1 to 500 py
1 to 2000 2y
Pinhole diameter = 4.00
Beam splitter position = 405/488/561/635 Quad
Number of averages = 1
Binning = 2x2
561 nm laser at 35% power
588 nm laser at 80% power
PMT 3 at 350 gain
Plate ID:642}

def extract_unformatted_data(raw)
  entry = {}

  # extract freetext block
  freetext, structured = raw.split("Pinhole diameter =")
  experiment_base_name = freetext.remove_first_line!
  second_line = freetext.remove_first_line!
  entry["freetext"] = freetext
    
  #extract all tag lines that are split by ":"
  with_double = raw.scan(/^.*:.*/)
  with_double.each do |line|
    tag, value = line.split(":")
    tag.strip!; value.strip!
    entry[tag] = value
  end

  #extract all tag lines that are split by "="
  with_eq = raw.scan(/^.*=.*/)
  with_eq.each do |line|
    tag, value = line.split("=")
    tag.strip!; value.strip!
    entry[tag] = value
  end

  #extract all tag lines with *nm laser*
  entry["Laser"] = raw.scan(/^.*nm laser.*/)

  #extract all tag lines with *1to*
  entry["Py"] = raw.scan(/^.*1 to.*/)

  #extract all tag lines with *at*gain*
  entry["Gain"] = raw.scan(/^.*at.*gain.*/)
  
  entry
end

def process
  hash = extract_unformatted_data(@@example_text)
  PP::pp(hash, $stderr, 50)
end

class String
  def remove_first_line!
    first_newline = (index("\n") || size - 1) + 1
    slice!(0, first_newline).sub("\n",'')
  end
end

process
