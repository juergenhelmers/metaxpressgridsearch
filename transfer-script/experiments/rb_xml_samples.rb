example_xml   = %{<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!-- Warning: this comment is an OME-XML metadata block, which contains crucial dimensional parameters and other important metadata. Please edit cautiously (if at all), and back up the original data before doing so. For more information, see the OME-TIFF web site: http://ome-xml.org/wiki/OmeTiff. -->
<OME xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.openmicroscopy.org/Schemas/OME/2010-06 http://www.openmicroscopy.org/Schemas/OME/2010-06/ome.xsd" xmlns="http://www.openmicroscopy.org/Schemas/OME/2010-06">
    <Instrument ID="urn:lsid:synbio.csir.co.za:Instrument:0">
        <Microscope Manufacturer="Molecular Devices" Model="ImageXpress Ultra Confocal HCS" SerialNumber="213123123" Type="Other"></Microscope>
        <Detector AmplificationGain="350.0" ID="urn:lsid:synbio.csir.co.za:Detector:PMT3" Manufacturer="Hamamatsu" Model="upper123" SerialNumber="123123213" Type="LifetimeImaging" Zoom="50.0"/>
        <LightSource ID="urn:lsid:synbio.csir.co.za:LightSource:Ultra-ls-001" Manufacturer="MD" Model="ls001" Power="35.00" SerialNumber="123123">
            <Laser Type="Semiconductor" LaserMedium="Ruby" Wavelength="561"></Laser>

        </LightSource>
    </Instrument>

    <Experiment ID="urn:lsid:synbio.csir.co.za:Experiment:some002" Type="Other">
        <ExperimenterRef ID="urn:lsid:synbio.csir.co.za:Experimenter:0"></ExperimenterRef>
    </Experiment>

    <Experimenter DisplayName="Juergen Helmers" Email="juergen.helmers@gmail.com" FirstName="Juergen" ID="urn:lsid:synbio.csir.co.za:Experimenter:0" Institution="CSIR" LastName="Helmers" UserName="root"></Experimenter>

    <Image ID="urn:lsid:synbio.csir.co.za:Image:xxx" Name="Image A01_w1">
        <AcquiredDate>2010-08-25T16:34:22</AcquiredDate>
        <Description>Optimization of encapsulation mix XPO1, overlaid with ATCC Hela for 48 hours, 1 x confluent T25 around three million cells per array, standard fix and label methods.
        ####
        This field should take all the original metadata can have a multiline text. All text in here will be found in searches...
        ####
        </Description>
        <InstrumentRef ID="urn:lsid:synbio.csir.co.za:Instrument:0"></InstrumentRef>
        <ExperimentRef ID="urn:lsid:synbio.csir.co.za:Experiment:some002"></ExperimentRef>
        <ImagingEnvironment Temperature="25.0"/>
        <Pixels DimensionOrder="XYCZT" ID="urn:lsid:synbio.csir.co.za:Pixels:0" PhysicalSizeX="1.6" PhysicalSizeY="1.6" PhysicalSizeZ="0.0" SizeC="1" SizeT="1" SizeX="1000" SizeY="1000" SizeZ="1" Type="uint16" >

            <Channel ID="urn:lsid:synbio.csir.co.za:Channel:0:0" Name="_metaXpress_" SamplesPerPixel="1" PinholeSize="4.0">
              <DetectorSettings Binning="2x2" ID="urn:lsid:synbio.csir.co.za:Detector:PMT3"/>
              <LightPath/>
           </Channel>


            <TiffData FirstC="0" FirstT="0" FirstZ="0" IFD="0" PlaneCount="1">
                <UUID FileName="import.ome.tif">
                    urn:uuid:35862478-279e-11e0-866b-001a4d4cda34
                </UUID>
            </TiffData>
            <Plane DeltaT="0.0" ExposureTime="0.0" TheC="0" TheT="0" TheZ="0"/>
        </Pixels>
    </Image>
</OME>

}