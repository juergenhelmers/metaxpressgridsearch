# script will check if a project of given name does exist.
# if not the project will be created.
# new dataset will be added to the existing or new project

require 'pg'
require 'dbi'
require 'escape'

# DB connection
@@db = DBI.connect("dbi:Pg:dbname=omero;host=localhost","omero", "IOP999jkl")

# global vars
# event
@@event_permissions = "-547"
@@event_type = 4
#session
@@session_id = @@db.select_all("SELECT id FROM session where id=(select max(id) from session)")[0][0]
#dataset
@@new_dataset_id = nil

def get_experimenter(login)
  return @@db.select_all("select id from experimenter WHERE omename='#{login}';")[0][0]
end 

def find_project(project_name,owner_id)
  return @@db.select_all("SELECT id FROM project WHERE name='#{project_name}' AND owner_id='#{owner_id}'")
end

def create_project(metadata,experimenter)
  puts "this function will create a new project"
  latest_event_id = @@db.select_all("SELECT id FROM event where time=(select max(id) from event)")
  new_event_id = latest_event_id[0][0] + 1
  new_event = @@db.select_all("INSERT INTO event (id, permissions, time, experimenter, experimentergroup, session, type) VALUES('#{new_event_id}', '#{@@event_permissions}', 'now', '#{experimenter["id"]}', '#{experimenter["group"][0][0]}', '#{@@session_id}', '#{@@event_type}')")
  project_description = "created: #{Time.now.strftime('%Y-%m-%d at %H:%M:%S')} by automated Omero import routine"
  latest_project_id = @@db.select_all("SELECT id FROM project where id=(select max(id) from project)")
  new_project_id = latest_project_id[0][0] + 1
  new_project = @@db.select_all("INSERT INTO project (id, description, permissions, name, group_id, owner_id, creation_id, update_id) VALUES('#{new_project_id}','#{project_description}','#{experimenter["permissions"]}','#{metadata["Experiment base name"]}','#{experimenter["group"][0][0]}','#{experimenter["id"]}', #{new_event_id}, #{new_event_id})")
  return new_project_id
end

def check_for_project(metadata,experimenter)

  project_id = find_project(metadata["Experiment base name"],experimenter["id"])
  #project_id = find_project(metadata["test"],experimenter["id"])
  if project_id.to_s.length == 0
    puts "project does NOT exist will create a new one:"
    return create_project(metadata,experimenter)
  else
    puts "project does exist"
    return project_id
  end
  
end

def create_event(experimenter)
  latest_event_id = @@db.select_all("SELECT id FROM event where id=(select max(id) from event)")[0][0]
  new_event_id = latest_event_id + 1
  new_event = @@db.select_all("INSERT INTO event (id, permissions, time, experimenter, experimentergroup, session, type) VALUES('#{new_event_id}', '#{@@event_permissions}', 'now', '#{experimenter["id"]}', '#{experimenter["group"][0][0]}', '#{@@session_id}', '#{@@event_type}')")
  return new_event_id
end

def create_dataset(metadata,experimenter)
  dataset_name = "#{Time.now.strftime("%Y-%m-%d")} + #{metadata["Experiment set"]}"
  #create final destination dataset for experimenter 
  new_event_id = create_event(experimenter)
  dataset_description = "created: #{Time.now.strftime("%Y-%m-%d at %H:%M:%S")} by automated Omero import routine"
  latest_dataset_id = @@db.select_all("SELECT id FROM dataset where id=(select max(id) from dataset)")
  new_dataset_id = latest_dataset_id[0][0] + 1
  new_dataset = @@db.select_all("INSERT INTO dataset (id, description, permissions, name, group_id, owner_id, creation_id, update_id) VALUES('#{new_dataset_id}','#{dataset_description}','#{experimenter["permissions"]}','#{metadata["Experiment set"]}','#{experimenter["group"][0][0]}','#{experimenter["id"]}', #{new_event_id}, #{new_event_id})")    
  return new_dataset_id
end

@@experimenter = {"id"=>"2", "firstname"=>"Juergen", "lastname"=>"Helmers", "email"=>"juergen.helmers@gmail.com", "institution"=>"CSIR", "permissions" => "-103"}
@@experimenter["group"] = @@db.select_all("SELECT parent FROM groupexperimentermap WHERE child_index='#{@@experimenter["id"]}' limit 1")
@@metadata = {"Experiment base name" => "2011016 test", "Experiment set" => "test fpr rbilly"  }
@@importer = {}
@@importer["id"] = "#{get_experimenter("importer")}" 
@@importer["group"] = @@db.select_all("SELECT parent FROM groupexperimentermap WHERE child='#{@@importer["id"]}' limit 1")[0][0]
@@importer["permissions"] = @@experimenter["permissions"]
final_project_id = check_for_project(@@metadata, @@experimenter)

#create dataset for experimenter 
dataset_id = create_dataset(@@metadata, @@importer)

#connect the new dataset to the importer project "currentimport"
import_project_id = find_project("Current Import",@@importer["id"])[0][0]
latest_link_id = @@db.select_all("SELECT id FROM projectdatasetlink where id=(select max(id) from projectdatasetlink)")[0][0]
new_projectdatasetlink_id = latest_link_id + 1
link_event_id = create_event(@@importer)

new_projectdatasetlink = @@db.select_all("INSERT INTO projectdatasetlink (id, permissions, child, creation_id, group_id, owner_id, update_id, parent) VALUES('#{new_projectdatasetlink_id}','#{@@experimenter["permissions"]}','#{dataset_id}','#{link_event_id}', '#{@@importer["group"]}', '#{@@importer["id"]}','#{link_event_id}', '#{import_project_id}')")
#
puts "call cmdline importer... with dataset ID of #{dataset_id}"

