class Parse

  def self.unformatted_metaexpress_metadata(raw)

    entry       = {}

    #extract all tag lines that are split by ":"
    with_double = raw.scan(/^.*:.*/)
    with_double.each do |line|
      tag, value = line.split(":")
      tag.strip!; value.strip!
      entry[tag] = value
    end

    #extract all tag lines that are split by "="
    with_eq = raw.scan(/^.*=.*/)
    with_eq.each do |line|
      tag, value = line.split("=")
      tag.strip!; value.strip!
      entry[tag] = value
    end

    #extract all tag lines with *nm laser*
    entry["Laser"] = raw.scan(/^.*nm laser.*/)
    entry["Laser"].each do |val|
      val.chomp!
    end

    #extract all tag lines with *1to*
    entry["Py"] = raw.scan(/^.*1 to.*/)
    entry["Py"].each do |val|
      val.chomp!
    end

    #extract all tag lines with *at*gain*
    entry["Gain"] = raw.scan(/^.*at.*gain.*/)
    entry["Gain"].each do |val|
      val.chomp!
    end

    entry
  end

  def self.exif_tags(raw)
    # extracts a nice ruby hash from the flat file exiv2 extract
    # result: exifentry hash contains all key, value pairs of metadata
    # for example entry["ImageDescription"] could be the xml of an OME-TIF
    entry     = {}
    # split string data into single exif tags
    tag_lines = raw.split("Exif.Image.")

    tag_lines.each do |line|
      line.chop! # chop off \n
      line.gsub!(/ {2,}/, "\t") # replace more than one space into tab for consistent splitting
      name, datatype, count, data = line.split("\t") # split at \n for different data subsets
      entry[name] = data # add tag data to result hash
    end

    entry
  end
end

