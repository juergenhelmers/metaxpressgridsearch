class Databases

  def initialize(omero_db = {}, metaexpress_db = {})

    begin
      @omero_connection = DBI.connect("dbi:Pg:dbname="+omero_db[:dbname]+";host="+omero_db[:host],
                                      omero_db[:user],
                                      omero_db[:password])
    rescue DBI::DatabaseError => e
      throw "Connection to Omero DB failed | Error code: #{e.err} | Error message: #{e.errstr} Error SQLSTATE: #{e.state}"
    end

    begin
      @metaexpress_connection = DBI.connect(metaexpress_db[:connection],
                                            metaexpress_db[:user], metaexpress_db[:password])
    rescue DBI::DatabaseError => e
      throw "Connection to MetaXpress database failed | Error code: #{e.err} | Error message: #{e.errstr} Error SQLSTATE: #{e.state}"
    end

    # event
    @event_permissions = "-547"
    @event_type = 4

    #session
    begin
      @session_id = @omero_connection.select_all("SELECT id FROM session where id=(select max(id) from session)")[0][0]
    rescue DBI::DatabaseError => e
      throw "session_id count to be obtained | Error code: #{e.err} | Error message: #{e.errstr} Error SQLSTATE: #{e.state}"
    end

    #dataset
    @new_dataset_id = nil

  end

  def get_experimenter(login)

    begin
      @omero_connection.select_all("select id from experimenter WHERE omename='#{login}';")[0][0]
    rescue DBI::DatabaseError => e
      throw "Experimenter ID not found | Error code: #{e.err} | Error message: #{e.errstr} Error SQLSTATE: #{e.state}"
    end

  end

  def get_importer(experimenter)

    importer = {}

    begin
      importer["id"] = @omero_connection.select_all("SELECT id FROM experimenter WHERE omename='importer';")[0][0]
    rescue DBI::DatabaseError => e
      throw "Importer not found | Error code: #{e.err} | Error message: #{e.errstr} Error SQLSTATE: #{e.state}"
    end
    begin
      importer["group"] = @omero_connection.select_all("SELECT parent FROM groupexperimentermap WHERE child='#{importer["id"]}' limit 1")[0][0]
    rescue DBI::DatabaseError => e
      throw "Importer group not found | Error code: #{e.err} | Error message: #{e.errstr} Error SQLSTATE: #{e.state}"
    end

    importer["permissions"] = experimenter["permissions"]
    importer

  end

  def find_project(project_name, owner_id)

    begin
      @omero_connection.select_all("SELECT id FROM project WHERE name='#{project_name}' AND owner_id='#{owner_id}'")
    rescue DBI::DatabaseError => e
      throw "Project not found | Error code: #{e.err} | Error message: #{e.errstr} Error SQLSTATE: #{e.state}"
    end

  end

  # get the username using the plateID from the image meta-data
  def get_metaexpress_user_id(plate_id)

    begin
      plates = @metaexpress_connection.select_all("select creator_id from plates WHERE plate_id='#{plate_id}'")
    rescue DBI::DatabaseError => e
      throw "metaXpress plate not found | Error code: #{e.err} | Error message: #{e.errstr} Error SQLSTATE: #{e.state}"
    end
    creator_id = plates[0][0]
    begin
      user = @metaexpress_connection.select_all("select login from users WHERE user_login_id='#{creator_id}'")
    rescue DBI::DatabaseError => e
      throw "metaXpress user not found | Error code: #{e.err} | Error message: #{e.errstr} Error SQLSTATE: #{e.state}"
    end
    user[0][0]
  end

  def get_omero_experimenter(login)

    begin
      query = @omero_connection.execute("select * from experimenter WHERE omename='#{login}';")
    rescue DBI::DatabaseError => e
      throw "Experimenter not found | Error code: #{e.err} | Error message: #{e.errstr} Error SQLSTATE: #{e.state}"
    end
    query.fetch_hash do |experimenter|
      experimenter["group"] = @omero_connection.select_all("SELECT parent FROM groupexperimentermap WHERE child=#{experimenter["id"]} limit 1")[0][0]
      return experimenter
    end
    query.finish

  end

  def check_for_project(metadata, experimenter)

    project_id = find_project(metadata["Experiment base name"], experimenter["id"])
    if project_id[0].to_s.length == 0
      create_project(metadata, experimenter)
    else
      project_id[0][0]
    end

  end

  def create_project(metadata, experimenter)

    begin
      experimenter["group"] = @omero_connection.select_all("SELECT parent FROM groupexperimentermap WHERE child=#{experimenter["id"]} limit 1")[0][0]
    rescue DBI::DatabaseError => e
      throw "Experimenter group not found | Error code: #{e.err} | Error message: #{e.errstr} Error SQLSTATE: #{e.state}"
    end
    new_event_id = create_event(experimenter)
    project_description = "created: #{Time.now.strftime('%Y-%m-%d at %H:%M:%S')} by automated Omero import routine"
    new_project_id = @omero_connection.select_all("SELECT nextval('seq_project')")[0][0]
    begin
      @omero_connection.select_all("INSERT INTO project (id, description, permissions, name, group_id, owner_id, creation_id, update_id) VALUES('#{new_project_id}','#{project_description}','#{experimenter["permissions"]}','#{metadata["Experiment base name"]}','#{experimenter["group"][0][0]}','#{experimenter["id"]}', #{new_event_id}, #{new_event_id})")
    rescue DBI::DatabaseError => e
      throw "New project could not be created | Error code: #{e.err} | Error message: #{e.errstr} Error SQLSTATE: #{e.state}"
    end
    new_project_id[0][0]

  end

  def create_event(experimenter)
    new_event_id = @omero_connection.select_all("SELECT nextval('seq_event')")[0][0]
    begin
      new_event = @omero_connection.select_all("INSERT INTO event (id, permissions, time, experimenter, experimentergroup, session, type) VALUES('#{new_event_id}', '#{@event_permissions}', 'now', '#{experimenter["id"]}', '#{experimenter["group"][0][0]}', '#{@session_id}', '#{@event_type}')")
    rescue DBI::DatabaseError => e
      throw "New event could not be created| Error code: #{e.err} | Error message: #{e.errstr} Error SQLSTATE: #{e.state}"
    end
    new_event_id
  end

  def create_dataset(metadata, importer)

    # dataset name is unused - why?
    dataset_name = "#{Time.now.strftime("%Y-%m-%d")}-#{metadata["Experiment set"].gsub(" ", "_")}"

    #create final destination dataset for importer
    new_event_id = create_event(importer)
    dataset_description = "created: #{Time.now.strftime("%Y-%m-%d at %H:%M:%S")} by automated Omero import routine"

    new_dataset_id = @omero_connection.select_all("SELECT nextval('seq_dataset')")[0][0]
    begin
      @omero_connection.select_all("INSERT INTO dataset (id, description, permissions, name, group_id, owner_id, creation_id, update_id) VALUES('#{new_dataset_id}','#{dataset_description}','#{importer["permissions"]}','#{metadata["Experiment set"]}','#{importer["group"][0][0]}','#{importer["id"]}', #{new_event_id}, #{new_event_id})")
    rescue DBI::DatabaseError => e
      throw "New dataset could not be created| Error code: #{e.err} | Error message: #{e.errstr} Error SQLSTATE: #{e.state}"
    end

    #connect the new dataset to the importer project "currentimport"
    import_project_id = find_project("Current Import", importer["id"])
    new_projectdatasetlink_id = @omero_connection.select_all("SELECT nextval('seq_projectdatasetlink')")[0][0]
    link_event_id = create_event(importer)

    begin
      @omero_connection.select_all("INSERT INTO projectdatasetlink (id, permissions, child, creation_id, group_id, owner_id, update_id, parent) VALUES('#{new_projectdatasetlink_id}','#{importer["permissions"]}','#{new_dataset_id}','#{link_event_id}', '#{importer["group"]}', '#{importer["id"]}','#{link_event_id}', '#{import_project_id[0][0]}')")
    rescue DBI::DatabaseError => e
      throw "New projectdatasetlink could not be created| Error code: #{e.err} | Error message: #{e.errstr} Error SQLSTATE: #{e.state}"
    end

    new_dataset_id
  end

end