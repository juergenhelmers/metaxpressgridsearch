class Generate
# define variables to be used in the OME-block

# microscope
  @@MicroscopeManufacturer  = "Molecular Devices"
  @@MicroscopeModel         = "ImageXpress Ultra Confocal HCS"
  @@MicroscopeSerialNumber  = "1234567"
  @@MicroscopeType          = "Other"

# detector
  @@DetectorManufacturer    = "Hamamatsu"
  @@DetectorModel           = "Model123"
  @@DetectorSerialNumber    = "1234567"
  @@DetectorType            = "LifetimeImaging"
  @@DetectorZoom            = "50.0"

#lightsource
  @@LightSourceManufacturer = "Biorad"
  @@LightSourceModel        = "Model 123123"
  @@LightSourceSerialNumber = "8343453"

#laser
  @@LaserMedium             = "Ruby"
  @@LaserType               = "Semiconductor"

  @@empty_xml               = %{<?xml version="1.0" encoding="UTF-8" standalone="no"?><OME xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.openmicroscopy.org/Schemas/OME/2010-06 http://www.openmicroscopy.org/Schemas/OME/2010-06/ome.xsd"
       xmlns="http://www.openmicroscopy.org/Schemas/OME/2010-06"></OME>}



  def self.generate_ome_xml(metadata, metaXpress_login, experimenter, image_number, uuid, filename)

    doc        = REXML::Document.new @@empty_xml

    # instrument information
    instrument = doc.elements['//OME'].add_element('Instrument', {
        'ID' => 'urn:lsid:synbio.csir.co.za:Instrument:0'})
    microscope = instrument.add_element('Microscope', {
        'Manufacturer' => @@MicroscopeManufacturer,
        'Model'        => @@MicroscopeModel,
        'SerialNumber' => @@MicroscopeSerialNumber,
        'Type'         => @@MicroscopeType})

    #loop through gain array for detectors
    metadata["Gain"].each do |gains|
      #"Gain"=>["PMT 4 at 600 gain"
      detector, number, bla, gain, bli = gains.split(" ")
      instrument.add_element('Detector', {
          'ID'                => 'urn:lsid:synbio.csir.co.za:Detector:'+detector+number,
          'AmplificationGain' => gain.to_f,
          'Manufacturer'      => @@DetectorManufacturer,
          'Model'             => @@DetectorModel,
          'SerialNumber'      => @@DetectorSerialNumber,
          'Type'              => @@DetectorType,
          'Zoom'              => @@DetectorZoom})
    end

    #loop lasers
    metadata["Laser"].each_with_index do |laser, index|
      #"488 nm laser at 60% power"
      wavelength, bla, blo, bli, power = laser.split(" ")
      power.gsub!("%", "")
      light_source = instrument.add_element('LightSource', {
          'ID'           => 'urn:lsid:synbio.csir.co.za:LightSource:'+index.to_s,
          'Manufacturer' => @@LightSourceManufacturer,
          'Model'        => @@LightSourceModel,
          'Power'        => power.to_f,
          'SerialNumber' => @@LightSourceSerialNumber})
      light_source.add_element('Laser', {
          'Type'        => @@LaserType,
          'LaserMedium' => @@LaserMedium,
          'Wavelength'  => wavelength})
    end

    # experiment information
    experiment = doc.elements['//OME'].add_element('Experiment', {
        'ID' => 'urn:lsid:synbio.csir.co.za:Experiment:0', 'Type' => 'Other'})

    #experimenter reference
    experiment.add_element('ExperimenterRef', {'ID' =>'urn:lsid:synbio.csir.co.za:Experimenter:' + experimenter["id"]})

    # experimenter information
    experiment = doc.elements['//OME'].add_element('Experimenter',
                                                   'ID'         =>'urn:lsid:synbio.csir.co.za:Experimenter:' + experimenter["id"],
                                                   'DisplayName'=>experimenter["firstname"] + " " + experimenter["lastname"],
                                                   'Email'      =>experimenter["email"],
                                                   'FirstName'  =>experimenter["firstname"],
                                                   'LastName'   =>experimenter["lastname"],
                                                   'Institution'=>experimenter["institution"], 'UserName'=> metaXpress_login)

    #image data
    #
    ## FIXIT
    #
    # Name should relate to the image file name (A01_w1)not to the dataset as it is displayed to the
    # user as the image name
    image      = doc.elements['//OME'].add_element('Image', {
        'ID'   => 'urn:lsid:synbio.csir.co.za:Image:'+image_number.to_s,
        'Name' => metadata["Experiment set"].to_s.gsub!(" ", "_")})
    image.add_element('AcquiredDate').add_text(metadata["DateTime"])
    image.add_element('Description').add_text(metadata["ImageDescription"])
    image.add_element('InstrumentRef', 'ID' => 'urn:lsid:synbio.csir.co.za:Instrument:0')
    image.add_element('ExperimentRef', 'ID' => 'urn:lsid:synbio.csir.co.za:Experiment:0')
    image.add_element('ImagingEnvironment', 'Temperature' => '25.0')

    #pixels
    pixels  = image.add_element('Pixels',
                                'ID'            =>'urn:lsid:synbio.csir.co.za:Pixels:0',
                                'DimensionOrder'=>'XYCZT',
                                'PhysicalSizeX' => '1.6', 'PhysicalSizeY' => '1.6', 'PhysicalSizeZ' => '0.0',
                                'SizeC'         => '1', 'SizeT'=> '1', 'SizeX'=> metadata["ImageWidth"], 'SizeY'=> metadata["ImageLength"], 'SizeZ'=> '1',
                                'Type'          => 'uint16')
    channel = pixels.add_element('Channel',
                                 'ID'              => 'urn:lsid:synbio.csir.co.za:Channel:0',
                                 'Name'            => '_metaXpress_',
                                 'PinholeSize'     => metadata["Pinhole diameter"],
                                 'SamplesPerPixel' => metadata["SamplesPerPixel"])
    metadata["Gain"].each do |gains|
      detector, number, bla, gain, bli = gains.split(" ")
      channel.add_element('DetectorSettings',
                          'ID'      => 'urn:lsid:synbio.csir.co.za:Detector:'+detector+number,
                          'Binning' => metadata["Binning"])
    end
    channel.add_element('LightPath')

    # tiff data
    tiff_data = pixels.add_element('TiffData', 'FirstC' => '0', 'FirstT' => '0', 'FirstZ' => '0', 'IFD' => '0', 'PlaneCount' => '1')
    tiff_data.add_element('UUID', 'FileName' => filename).add_text('urn:uuid:'+uuid)
    pixels.add_element('Plane', 'DeltaT' => '0.0', 'ExposureTime' => '0', 'TheC' => '0', 'TheT' => '0', 'TheZ' => '0')

    doc # return xmnl document data structure
  end

end
