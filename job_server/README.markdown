1) bundle install
2) install resque/redis: git clone git://github.com/defunkt/resque.git
3) install redis: cd resque; PREFIX=/opt/redis rake redis:install
4) start redis: PREFIX=/opt/redis rake redis:start
5) start web-app (other window, same gemset): rake start
   start server in debug mode: thin start -p 9292 -R config.ru -D -V
6) start some worker threads (other window): VERBOSE=true QUEUE=default,reassignment COUNT=8 rake resque:workers
7) see admin gui at http://localhost:9292/
8) run transfer.rb script to enqueue image import batches
