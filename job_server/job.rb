require 'resque'
require 'escape'
require 'pg'
require 'dbi'


module OmeroImporter

  module OmeroImporterJob

    #  Command Pattern based execution of scheduled jobs
    #
    #  - currently only 'import' is used but all kind of remotely
    #    triggered operations (shell and other) are possible

    @queue = :default
    @working_dir = '/home/helmerj/Downloads/OMERO.clients-Beta4.2.2.linux/'
    #@working_dir = '/home/helmerj/Downloads/trunk/target/OMERO.importer-4.3.0-DEV/'

    def self.process_import(data)

      # redirect stdout and stderr to log files
      $stdout.reopen($?.pid.to_s+".stdout.log", "w")
      $stderr.reopen($?.pid.to_s+".stderr.log", "w")

      # bail out if an error occured in whole import process - fail early!
      if Resque::Failure.count > 0
        throw "Error: Failures in Queing System - stopping further import processing!!!"
      end


      dataset_id = data["dataset_id"]
      files = data["files"]
      omero_host = data["omero_host"]

      importer_cmd_line = ["./importer-cli", "-s", "#{omero_host}", "-u", "importer", "-w", "IOP999jkl", "-r", dataset_id.to_s]

      files.each_with_index do |file, index|

        #convert .tif files to .ome.tif and put ome-tif xml document into output image
        input_filename = data["input_directory"]+file
        output_filename = data["output_directory"]+file.gsub(".tif", ".ome.tif")
        convert_command = Escape.shell_command(["convert",
                                                "-depth", "16",
                                                "-comment", "#{data["docs"][index]}",
                                                input_filename, output_filename])
        #execute convert for an image
        %x[#{convert_command} 2> /dev/null]
        # check for errors of convert command
        throw "convert failed for image "+input_filename+" Error: " + $?.to_s + " Command: " + convert_command unless $?.success?

        #add converted image to importer command line
        importer_cmd_line << output_filename
      end

      command = Escape.shell_command(importer_cmd_line)
      # change effective working dir for stupid python-script and execute importer
      %x[cd #{@working_dir}; #{command}]
      #%x["ls"]
      result_code = $?
      if result_code.to_i == 0
=begin
        #TODO - deletions should now be done after success with reassignment

        # delete successfully imported files
        files.each do |file|
          #delete original .tif files
          File.delete(data["input_directory"]+file)
          #delete converted .ome.tif files
          File.delete(data["output_directory"]+file.gsub(".tif", ".ome.tif"))
        end
=end
      else
        error_message = "Error processing Omero Importer job with cmd line:" + command
        # move/throw this job to :failure queue
        throw "Error: " + error_message
      end
    end

    def self.perform(command_hash)

      # branch according to command type
      case command_hash["command"]

        # import omero xml image batch
        when "import" then
          process_import(command_hash)

        # test for generating workload on workers
        when "sleep" then
          command = Escape.shell_command(["sleep", '10'])
          result = %x[#{command}]
        # sleep 10

      end

    end
  end

  module OmeroDatabaseReassignment

    @queue = :reassignment

    # number of "real" workers" without reassignment workers
    def self.worker_count

      #subtract reassignment workers from the overall worker count
      worker_count = Resque::working.size
      Resque::working.each do |worker|
        if worker.job["queue"].eql?("reassignment") or
            !worker.working?
          worker_count = worker_count - 1
        end
      end
      worker_count

    end

    def self.reassign_dataset(data)

      dataset_id = data["dataset_id"]
      experimenter = data["experimenter"]
      final_project_id = data["final_project_id"]

      db = DBI.connect("dbi:Pg:dbname=omero;host=localhost", "omero", "IOP999jkl")
      # change the owner and group of dataset
      db.select_all("UPDATE dataset SET owner_id=#{experimenter["id"]}, group_id=#{experimenter["group"]} WHERE id=#{dataset_id}")
      # reassign the dataset to the final project
      db.select_all("UPDATE projectdatasetlink SET parent=#{final_project_id} WHERE child=#{dataset_id}")
      # change owner and group id on datasetimagelink
      db.select_all("UPDATE datasetimagelink SET group_id=#{experimenter["group"]}, owner_id=#{experimenter["id"]} WHERE parent=#{dataset_id};")
      # change owner and group id of all images in dataset
      db.select_all("UPDATE image SET group_id=#{experimenter["group"]},owner_id=#{experimenter["id"]} WHERE id IN (SELECT child FROM datasetimagelink WHERE parent=#{dataset_id});")

    end

    def self.process_reassignment(data)

      # bail out if an error occured in whole import process - fail early!
      if Resque::Failure.count > 0
        throw "Error: Failures in Queing System - stopping further import processing!!!"
      end

      #wait until queue is empty and all import workers are done
      while Resque::size(:default) > 0 or worker_count > 0
        #Resque::working.each do |worker|
        #  p "Working on: " + worker.job.to_s
        #end
        sleep 30
      end

      # do reassignment of import job data if no errors occurred
      if Resque::Failure.count == 0
        reassign_dataset(data)
        #p "Simulated reassign success."
        #TODO - after successful reassign the images (in input & output) should be deleted
      else
        throw "ERROR - Cancelling db reassignment - #{Resque::Failure.count.to_s} failed jobs"
        #TODO - delete datasets for failed import batch from omero database
        #TODO - images in output could also be deleted here
      end

    end

    def self.perform(command_hash)
      case command_hash["command"]
        when "reassignment" then
          process_reassignment(command_hash)
      end
    end
  end

end
