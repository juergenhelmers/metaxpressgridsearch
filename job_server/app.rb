require 'sinatra/base'
require 'resque'
require 'escape'
require 'pg'
require 'dbi'
require File.expand_path(File.dirname(__FILE__) + '/job.rb')


module OmeroImporter
  class App < Sinatra::Base

    @@auth_key = "gghhddeeffss7657654gff54444"

    post '/' do
      data = JSON.parse(request.env["rack.input"].read)
      if !data.nil? and data["auth_key"].eql?(@@auth_key)
        Resque.enqueue(OmeroImporterJob, data)
        status 200
      else
        status 401 # return http authentication error
      end
    end

    post '/reassign' do
      data = JSON.parse(request.env["rack.input"].read)
      if !data.nil? and data["auth_key"].eql?(@@auth_key)
        Resque.enqueue(OmeroDatabaseReassignment, data)
        status 200
      else
        status 401 # return http authentication error
      end
    end

    # retry all jobs in failure queue
    get '/requeue' do
      # Requeue all jobs in the failed queue
      (Resque::Failure.count-1).downto(0).each { |i| Resque::Failure.requeue(i) }
      redirect '/'
    end

    # delete all jobs in failure queue
    get '/cleanup' do
      # Clear the failed queue
      Resque::Failure.clear
      redirect '/'
    end

    # redirect to resque admin gui
    get '*' do
      redirect '/resque'
    end

  end
end
