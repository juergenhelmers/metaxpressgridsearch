#!/usr/bin/env ruby
#require 'rubygems'
require 'net/http'
require 'json'

@host = 'localhost'
@port = '9292'
@post_ws = "/"
@payload ={
    "command" => "import",
    "files" => ["f 1 .tif", "f 2", "f  3"],
    "auth_key" => "gghhddeeffss7657654gff54444"
}

def post(payload)
  req = Net::HTTP::Post.new(@post_ws, {'Content-Type' =>'application/json'})
  #req.basic_auth @user, @pass
  req.body = payload
  response = Net::HTTP.new(@host, @port).start { |http| http.request(req) }
  #puts "Response Code: #{response.code} Response Message: #{response.message}: - Body: #{response.body}"
end

1.upto(20) do |x|
  post(@payload.to_json)

  # add demo file names
  #@payload["files"] << x

  # try invalid json request
  #post('junk. no valid json string.')
end
